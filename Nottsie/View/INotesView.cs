﻿using System.Collections.Generic;

namespace Nottsie.View
{
    public interface INotesView
    {
        IList<string> NoteList { get; set; }
        int SelectedNote { get; set; }
        string Title { get; set; }
        string Contents { get; set; }
        uint Importance { get; set; }
        void DisplayDialog(string title, string message);
        Presenter.NotesPresenter Presenter { set; }
    }
}
