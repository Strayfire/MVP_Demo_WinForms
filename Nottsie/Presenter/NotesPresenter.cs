﻿using System;
using Nottsie.Model;
using Nottsie.View;
using System.Collections.Generic;
using System.Linq;

namespace Nottsie.Presenter
{
    public class NotesPresenter : INotesPresenter
    {
        private readonly INotesView view;
        private readonly INotesRepository repository;

        public NotesPresenter(INotesView view, INotesRepository repository)
        {
            this.view = view;
            this.repository = repository;
            this.view.Presenter = this;

            this.UpdateNotesListView();
        }

        public void UpdateNotesListView()
        {
            List<String> notes = repository.GetAllNotes().Select(s => s.Title).ToList();
            int selected = view.SelectedNote;
            view.NoteList = notes;
            view.SelectedNote = selected;
        }

        public void UpdateNotesView(int position)
        {
            if (position < 0)
                return;
            Note note = repository.GetNote(position);
            view.Title = note.Title;
            view.Contents = note.Text;
            view.Importance = note.Importance;
        }

        public void SaveNote()
        {
            try
            {
                Validate();
                repository.SaveNote(new Note(view.Title, view.Contents, view.Importance));
                this.UpdateNotesListView();
            }
            catch (Exception exception)
            {
                this.view.DisplayDialog("A problem has occured", exception.Message);
            }            
        }

        private void Validate()
        {
            if (string.IsNullOrEmpty(view.Contents))
                throw new InvalidOperationException("Empty message.");
            if (string.IsNullOrEmpty(view.Title))
                throw new InvalidOperationException("Empty title.");
        }
    }
}
