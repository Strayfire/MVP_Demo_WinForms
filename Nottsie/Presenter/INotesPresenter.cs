﻿namespace Nottsie.Presenter
{
    public interface INotesPresenter
    {
        void SaveNote();
        void UpdateNotesListView();
        void UpdateNotesView(int id);
    }
}
