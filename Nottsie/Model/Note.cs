﻿namespace Nottsie.Model
{
    public class Note
    {
        public string Title { get; private set; }
        public string Text { get; private set; }
        public uint Importance { get; private set; }

        public Note(string title, string text, uint importance)
        {
            this.Title = title;
            this.Text = text;
            this.Importance = importance;
        }

        public void IncreaseImportance() { ++this.Importance; }
        public void DecreaseImportance()
        {
            this.Importance -= this.Importance == 1U ? 1U : 0U;
        }
    }
}
